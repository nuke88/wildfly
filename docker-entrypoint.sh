#!/bin/bash

set -e

# if command starts with an option, prepend mysqld
if [ "${1:0:1}" = '-' ]; then
	set -- standalone "$@"
fi

init () {
	MODE=$1
	echo Init Wildfly ${WILDFLY_VERSION} - ${MODE}
	if [ "$WILDFLY_RELEASE_DATE" = $(stat -c %y ${WILDFLY_HOME}/${MODE}/configuration/mgmt-users.properties | grep -o -e "^[^[:space:]]\+") ]
	then
		echo "Add manager user: ${WILDFLY_MANAGER}"
		add-user.sh $WILDFLY_MANAGER $WILDFLY_MANAGER_PASSWORD
	fi;

}

check_env () {
	echo Check parameters
	: ${WILDFLY_MANAGER:?"Need to define WILDFLY_MANAGER"}
	: ${WILDFLY_MANAGER_PASSWORD:?"Need to define WILDFLY_MANAGER_PASSWORD"}
}


if [ "$1" = "standalone" ]
then
	check_env
	init "standalone"
	standalone.sh
fi;

