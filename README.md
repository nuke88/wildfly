Description
=================
[Wildfly](http://wildfly.org/about/ "Wildfly about") application server with OpenJDK 8 JRE.
Just a simple standalone 

Builds
------------
The builds are based on the latest openjdk8-jre. Current version is `openjdk:8u111-jre` [official](https://hub.docker.com/_/openjdk/).

I will support the latest major Final versions and the newest Candidate Release versions.

Supported versions:

* 9.0.0.Final

* 9.0.1.Final

* 9.0.2.Final

* 10.0.0.Final

* 10.1.0.Final, latest

  
Run
------------
You have to specify the management username and password with the `WILDFLY_MANAGER` and `WILDFLY_MANAGER_PASSWORD` environment variables and you are ready to use the server.

```
$ docker run --name wildfly -p 9990:9990 -p 8080:8080 -v /var/opt/wildfly/deployments:/opt/wildfly/standalone/deployments -e WILDFLY_MANAGER=manager -e WILDFLY_MANAGER_PASSWORD=secret vereshus/wildfly:latest
```


Configuration
------------
By default the management, the public and the unsecure interfaces are listening on `any-address` so change that property first to restrict the unauthorized access. [See](https://docs.jboss.org/author/display/WFLY9/Interfaces+and+ports "Wildfly interfaces and ports") more details

Use the management console to modify the settings or use either the `jboss-cli.sh` or `jboss-cli.bat` to connect to the server on 9990 port.  

Example:
```
[standalone@172.17.42.1:9990 /] /interface=public:undefine-attribute(name=any-address)
[standalone@172.17.42.1:9990 /] /interface=public:write-attribute(name=subnet-match,value=172.17.0.0/16)
[standalone@172.17.42.1:9990 /] /interface=management:undefine-attribute(name=any-address)
[standalone@172.17.42.1:9990 /] /interface=management:write-attribute(name=subnet-match,value=172.17.0.0/16)
[standalone@172.17.42.1:9990 /] /interface=unsecure:remove
[standalone@172.17.42.1:9990 /] :reload
```

Modules
------------
The modules folder is defined as docker `VOLUME` so feel free to add your own compatible modules to the system.

Deploy
------------
The deployment folder is either a `VOLUME` so you can copy your `WAR` into this folder and `touch` a `.war.dodeploy` file to trigger the server to deploy.
